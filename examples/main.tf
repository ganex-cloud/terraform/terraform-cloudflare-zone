module "cloudflare-zone_example-com" {
  source = "git::https://gitlab.com/ganex-cloud/terraform/terraform-cloudflare-zone.git?ref=master"
  zone   = "example.com"
  plan   = "free"
}
