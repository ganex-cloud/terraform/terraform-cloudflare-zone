output "this_id" {
  description = "The zone ID."
  value       = cloudflare_zone.this.id
}

output "this_plan" {
  description = "The name of the commercial plan to apply to the zone."
  value       = cloudflare_zone.this.plan
}

output "this_name_servers" {
  description = "Cloudflare-assigned name servers. This is only populated for zones that use Cloudflare DNS."
  value       = cloudflare_zone.this.name_servers
}
